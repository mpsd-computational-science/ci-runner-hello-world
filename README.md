# ci-runner-pool

This repository is used to set up and control all local runners provided at MPSD.
Individual runners can be activated in other repositories depending on the
required resources in these.

Users can select runners using `tags`. Runners marked as `run untagged=yes` will
be used by a job once the runner is activated in the repository. Runners marked
as `run untagged=no` only run jobs that have the required tag. Users are advised
to only use tags when required (untagged jobs will work in forks, tagged jobs
only if the fork as runners with the same tags).

## Available runners

### Jacamar

#### Debian 11 (bullseye) [deprecated]

| tags                                  | hosts                      | concurrent jobs | notes                                                                     | run untagged |
|---------------------------------------|----------------------------|-----------------|---------------------------------------------------------------------------|--------------|
| mpsd-sandybridge                      | mpsd-hpc-jacamar-001       | 10              | jobs submitted to public partition, reservation `jacamar1`                | no           |

#### Debian 12 (bookworm)

All nodes have the tags:
- debian12
- bookworm

| tags              | hosts                      | concurrent jobs | notes                                                                     | run untagged |
|-------------------|----------------------------|-----------------|---------------------------------------------------------------------------|--------------|
| mpsd-public       | mpsd-hpc-jacamar-003       | 2               | jobs submitted to public partition, reservation `jacamar-public-1`        | no           |
| mpsd-public2-12   | mpsd-hpc-jacamar-003       | 1               | jobs submitted to public2 partition, reservation `jacamar-public2-1`      | no           |
| mpsd-power8       | mpsd-hpc-jacamar-003       | 1               | jobs submitted to power8 partition, reservation `jacamar-power8-1`        | no           |
| mpsd-rtx2080ti    | mpsd-hpc-jacamar-003       | 1               | jobs submitted to rtx2080ti partition, reservation `jacamar-rtx2080ti-1`  | no           |
| mpsd-zen4-12      | mpsd-hpc-jacamar-003       | 1               | jobs submitted to zen4 partition, reservation `jacamar-zen4-1`            | no           |

#### Notes
[Jacamar](https://ecp-ci.gitlab.io/docs/admin/jacamar/introduction.html) is used
to submit CI jobs to the Slurm queue. The two *host* machines control the
communication and are reliable for job preparation (including cloning the
project) and cleanup. The `before_script` and `script` are submitted to Slurm
(as one job). Each job can define a variable `SCHEDULER_PARAMETERS` to request
the required resources.

**NOTE** Due to limitiations in Jacamar it is currently still necessary to
specify the partition and reservation as part of `SCHEDULER_PARAMETERS` to get
to the desired partition. See https://gitlab.com/ecp-ci/jacamar-ci/-/issues/161
for the current status.

GitLab runners are used on the Jacamar machines to perform all communication
with GitLab. Jacamar acts as a custom executor for the GitLab runner. A new
Jacamar process is started from the GitLab runner software when a job is
received. The Jacamar process ends when the job is finished. All Jacamar jobs
currently run as the dedicated user `cioctopu`.

Each CI job in Jacamar runs in a unique directory based on the CI job ID. Jobs do not
perform any cleanup at the end. A daily cronjob on `mpsd-hpc-jacamar` deletes
all job output older than 7 days.

### Docker runners

| tags                   | hosts                             | concurrent jobs | notes                                                         | run untagged |
|------------------------|-----------------------------------|-----------------|---------------------------------------------------------------|--------------|
| mpsd-docker            | mpsd-hpc-pizza-061                | 20              | 4 CPUs, 12 GB RAM per job                                     | yes          |
| mpsd-docker            | mpsd-vm-gitlab-runner-001         | 4               | 2 CPUs per job (8GB RAM shared between all concurrent jobs)   | yes          |
| mpsd-docker-large      | mpsd-srv-ibm-[001,002]            | 8               | 8 CPUs per job (500GB RAM shared between all concurrent jobs) | yes          |
| mpsd-docker-privileged | mpsd-gitlab-runner-privileged-001 | 1               | 12 CPUs, 16GB RAM, support for Docker-in-Docker               | no           |

Each docker container has access to limited CPU resources indicated in the table
above. At the moment no restrictions on the memory are applied, i.e. all
container share the entire RAM and could potentially conflict with each other.

*Note*: The CPU restrictions are not visible inside the docker container using
standard linux tools, e.g. `nprocs` would return the CPU count of the host
machine not the number of cores available in the container.

### Shell runners

| tags                       | hosts                     | concurrent jobs | notes                                               | run untagged |
|----------------------------|---------------------------|-----------------|-----------------------------------------------------|--------------|
| mpsd-octopus-web-uploader¹ | mpsd-octopus-web-uploader | 1               | Access to webserver, used to update website content | no           |
| mpsd-mac-m1                | mpsd-hpc-macm1-[001,002]  | 2               | Shell runner on Mac Mini                            | no           |

¹Dedicated VM with access to octopus-code.org webserver to update the website
inside a CI job. Only one job to prevent concurrent writes.

## Controlling runners

The configuration for all runners is managed in FAI. GitLab runner software
automatically changes upon runner configuration changes, no manual steps are
required. GitLab runners run as systemd services.

Multiple machines are connected to most of the available runners. Individual
machines can be paused by stopping the systemd service on the host. It is
configured such that it will finish the currently running jobs and not accept
new jobs when stop is issued.

A full runner (all associated machines) can also be paused (i.e. no new jobs are
accepted) in the GitLab web interface under `Settings -> CI/CD` (per repository).

## CI in this repository

The CI in this repository is used to check the status of all available runner types.

### .pre stage

One jobs is performed on each runner type, showing information on available
resources, host, ...

TODO Docker runners are not tested properly yet.

### Minimal test example

TODO NOT UP TO DATE

The test example is used both to test basic functionality of the different
runners and also to demonstrate how to use the different runners.

```console
cd src
make hello
make test
```
